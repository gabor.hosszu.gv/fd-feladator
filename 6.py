daily_temp_list = []

while len(daily_temp_list) < 7:
    temp = input("Kérem az adott napi átlag hőmérsékletet: ")
    try:
        daily_temp_list.append(float(temp))
    except Exception:
        print("Csak számokat fogadok el")

print(f"Heti átlag hőmérséklet: {sum(daily_temp_list)/7:.2f}")