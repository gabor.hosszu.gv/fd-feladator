numbers = [3.15, 5.9, 8.5, 8.9, 9.2, 14.0, 11.2, 28.2]
result = 0

for number in numbers:
    if number > 8:
        result += number

print(result)

# print(sum(x for x in numbers if x > 8))