"""
Természetes számokról el kell dönteni, hogy az polindrom-e vagy sem.
"""

def is_num_palindrom(num:int) -> bool:
    num_as_string = str(num)
    return num_as_string == num_as_string[::-1]

print(is_num_palindrom(1231))