"""
A börtönben 100 cella van  cellánként 1 zár, amely 2 állapotú
Az 1.szolga mindegyiket kinyitja, a 2.szolga minden másodikat fordítja el, majd a 100.szolga a 100.at fordítja el.
Mennyien szabadultak a börtönből?
"""

NUMBER_OF_DOORS = 100

doors = [False] * NUMBER_OF_DOORS

for i in range(1, NUMBER_OF_DOORS + 1):
    for j in range(i, NUMBER_OF_DOORS, i):
        doors[j-1] = not doors[j-1]

print(doors.count(True))
