numbers = [30.154, 35.924, 38.512, 38.129, 29.132, 24.002, 21.342, 18.201]
result = 0

for number in numbers:
    if number < 30:
        result += number

print(round(result, 3))

# print("{:.3f}".format(sum(x for x in numbers if x < 30)))