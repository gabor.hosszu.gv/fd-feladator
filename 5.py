results = 0
while True:
    income = input("Következő bevétel: ")
    if income == '0':
        break
    try:
        results += float(income)
    except Exception:
        print("Csak számokat fogadok el!")

print(f"A végeredmény: {results}")