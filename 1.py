"""
Írassuk ki a 3 jegyű Armstrong-számokat.
"""
# NOOB: tizedes jegyeken alapuló megoldás 3 jegyű számokra
for n in range(100, 1000):
    c = n//100
    x = n//10 - c * 10
    i = n - c*100 - x*10
    if c**3 + x**3 + i**3 == n:
        print(n)

# MEDI: string-esítő általános megoldás
for n in range(100, 1000):
    digit_list= []
    number_string = str(n)
    for i in number_string:
        digit_list.append(int(i))
    powered_digits = map(lambda x: x**len(number_string), digit_list)
    if sum(powered_digits) == n:
        print(n)